import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { studentFormActions } from "../storeToolkit/Form/slice";

const ResultForm = () => {
  const { studentList } = useSelector((state) => state.btForm);
  const dispatch = useDispatch();
  console.log("studentList", studentList);
  return (
    <div className="mt-3" style={{ textAlign: "center" }}>
      <form
        className="form-inline my-2 my-lg-0"
        onSubmit={(event) => {
          const search = document.getElementById("search").value;
          console.log("search", search);
          event.preventDefault();
          dispatch(studentFormActions.searchStudent(search));
        }}
      >
        <input
          className="form-control mr-sm-2"
          type="search"
          id="search"
          placeholder="Search"
          aria-label="Search"
        />
        <button className="btn btn-outline-success my-2 my-sm-0">Search</button>
      </form>

      <table className="table mt-3">
        <thead className="bg-dark px-2 py-2 text-white ">
          <tr>
            <th>Mã SV</th>
            <th>Họ tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {studentList?.map((item) => (
            <tr key={item?.id}>
              <td>{item?.id}</td>
              <td>{item?.hoTen}</td>
              <td>{item?.sdt}</td>
              <td>{item?.email}</td>
              <td>
                <button
                  className="btn btn-danger mr-3"
                  onClick={() => {
                    dispatch(studentFormActions.deleteStudent(item.id));
                  }}
                >
                  Delete
                </button>
                <button
                  className="btn btn-info"
                  onClick={() => {
                    dispatch(studentFormActions.editStudent(item));
                  }}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ResultForm;
