import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { studentFormActions } from "../storeToolkit/Form/slice";

const InforForm = () => {
  const [formData, setFormData] = useState();
  const [formError, setFormError] = useState();
  const dispatch = useDispatch();
  const { studentEdit } = useSelector((state) => state.btForm);
  const { studentId } = useSelector((state) => state.btForm);
  const { studentList } = useSelector((state) => state.btForm);
  const handleFormData = () => (ev) => {
    const { name, value, minLength, max, title, validity } = ev.target;
    let mess;
    if (minLength !== -1 && !value.length) {
      mess = `Vui lòng không để trống ${title}`;
    } else if (value.length < minLength) {
      mess = `Vui lòng nhập tối thiểu ${minLength} kí tự số`;
    } else if (value.length > max && max) {
      mess = `Vui lòng nhập tối đa ${max} kí tự số`;
    } else if (validity.patternMismatch && name === "id") {
      mess = `Vui lòng nhập kí tự là số`;
    } else if (validity.patternMismatch && name === "sdt") {
      mess = `Vui lòng nhập kí tự là số`;
    } else if (validity.patternMismatch && name === "email") {
      mess = `Vui lòng nhập đúng email`;
    }
    if (studentId) {
      studentList.forEach((item) => {
        if (value !== item.id) {
          dispatch(studentFormActions.setUpId(undefined));
        }
      });
    }

    setFormError({
      ...formError,
      [name]: mess,
    });
    setFormData({ ...formData, [name]: mess ? undefined : value });
  };
  useEffect(() => {
    if (!studentEdit) return;
    setFormData(studentEdit);
  }, [studentEdit]);
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        const elements = document.querySelectorAll("input");
        let formError = {};
        elements.forEach((ele) => {
          const { name, value, minLength, max, title, validity } = ele;
          let mess;
          if (minLength !== -1 && !value.length) {
            mess = `Vui lòng không để trống ${title}`;
          } else if (value.length < minLength) {
            mess = `Vui lòng nhập tối thiểu ${minLength} kí tự số`;
          } else if (value.length > max && max) {
            mess = `Vui lòng nhập tối đa ${max} kí tự số`;
          } else if (validity.patternMismatch && name === "id") {
            mess = `Vui lòng nhập kí tự là số`;
          } else if (validity.patternMismatch && name === "sdt") {
            mess = `Vui lòng nhập kí tự là số`;
          } else if (validity.patternMismatch && name === "email") {
            mess = `Vui lòng nhập đúng email`;
          }
          formError[name] = mess;
        });

        let flag = false;

        for (let key in formError) {
          if (formError[key]) {
            flag = true;
            break;
          }
        }
        if (flag) {
          setFormError(formError);
          return;
        }
        if (studentEdit) {
          dispatch(studentFormActions.updateStudent(formData));
        } else {
          dispatch(studentFormActions.addStudent(formData));
        }
      }}
      noValidate
    >
      <h3 className="bg-dark px-2 py-2 text-white">Thông tin thông viên</h3>
      <div className="row form-group">
        <div className="col-6">
          <p className="mb-0">Mã SV</p>
          <input
            type="text"
            name="id"
            disabled={!!studentEdit}
            value={formData?.id}
            title="mã sinh viên"
            minLength={1}
            max={6}
            pattern="^[0-9]+$"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger" id="id">
            {formError?.id || studentId}
          </p>
        </div>
        <div className="col-6">
          <p className="mb-0">Họ tên</p>
          <input
            type="text"
            value={formData?.hoTen}
            name="hoTen"
            title="họ tên"
            minLength={1}
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.hoTen}</p>
        </div>
      </div>
      <div className="row form-group mt-3">
        <div className="col-6">
          <p className="mb-0">Số điện thoại</p>
          <input
            type="text"
            name="sdt"
            value={formData?.sdt}
            title="số điện thoại"
            minLength={10}
            max={11}
            pattern="^[0-9]+$"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.sdt}</p>
        </div>
        <div className="col-6">
          <p className="mb-0">Email</p>
          <input
            type="text"
            value={formData?.email}
            name="email"
            title="email"
            minLength={1}
            pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.email}</p>
        </div>
      </div>
      <div className="mt-3">
        {studentEdit && <button className="btn btn-info">Cập nhật</button>}
        {!studentEdit && (
          <button className="btn btn-success mr-3">Thêm sinh viên</button>
        )}
      </div>
    </form>
  );
};

export default InforForm;
