import React from "react";
import InforForm from "./InforForm";
import ResultForm from "./ResultForm";

const MainForm = () => {
  return (
    <div className="container">
      <h1 className="text-center">BT ReactForm</h1>
      <InforForm />
      <ResultForm />
    </div>
  );
};

export default MainForm;
