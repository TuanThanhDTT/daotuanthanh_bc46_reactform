import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  studentList: [],
  studentEdit: undefined,
  studentId: undefined,
};
const studentFormSlice = createSlice({
  name: "studentForm",
  initialState,
  reducers: {
    addStudent: (state, action) => {
      if (state.studentList) {
        let flag = false;
        state.studentList.forEach((item) => {
          if (item.id === action.payload.id) {
            return (flag = true);
          }
        });
        if (flag) {
          state.studentId = "Mã sinh viên trùng";
        } else {
          state.studentList.push(action.payload);
        }
      }
    },
    setUpId: (state, action) => {
      state.studentId = action.payload;
    },
    deleteStudent: (state, action) => {
      state.studentList = state.studentList.filter(
        (item) => item.id !== action.payload
      );
    },
    editStudent: (state, action) => {
      state.studentEdit = action.payload;
    },
    updateStudent: (state, action) => {
      state.studentList = state.studentList.map((item) => {
        if (item.id === action.payload.id) {
          return (item = action.payload);
        }
        return item;
      });
      state.studentEdit = undefined;
    },
    searchStudent: (state, action) => {
      state.studentList.forEach((item) => {
        if (item.id === action.payload) {
          state.studentList = state.studentList.filter((item) => {
            return item.id === action.payload;
          });
        } else if (item.hoTen === action.payload) {
          state.studentList = state.studentList.filter((item) => {
            return item.hoTen === action.payload;
          });
        } else if (item.sdt === action.payload) {
          state.studentList = state.studentList.filter((item) => {
            return item.sdt === action.payload;
          });
        } else if (item.email === action.payload) {
          state.studentList = state.studentList.filter((item) => {
            return item.email === action.payload;
          });
        }
      });
    },
  },
});
export const { actions: studentFormActions, reducer: studentFormReducer } =
  studentFormSlice;
