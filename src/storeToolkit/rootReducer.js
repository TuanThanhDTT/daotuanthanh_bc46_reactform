import { combineReducers } from "@reduxjs/toolkit";
import { studentFormReducer } from "./Form/slice";

export const rootReducer = combineReducers({
  btForm: studentFormReducer,
});
